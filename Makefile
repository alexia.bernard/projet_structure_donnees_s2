SRCPATH = src/kruskal

default : compile

compile :
	javac $(SRCPATH)/*.java $(SRCPATH)/unionfind/*.java

run :
	java -cp src kruskal.Main $(args)

all: clean compile run

clean :
	rm -f $(SRCPATH)/*.class $(SRCPATH)/unionfind/*.class 
