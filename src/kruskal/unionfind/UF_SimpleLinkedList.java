package kruskal.unionfind;

import java.util.ArrayList;
import java.util.HashMap;

import kruskal.LinkedList;
import kruskal.Sommet;

/**
 * Implémentation de l'interface UnionFind avec des listes simplements chaînées.
 */
public class UF_SimpleLinkedList implements UnionFind
{
    /**
     * La liste des représentants des différents ensembles
     */
    private ArrayList<Sommet> representants;
    private HashMap<Sommet, LinkedList<Sommet>> representing;


    /**
     * Constructeur initialisant la liste des représentants
     */
    public UF_SimpleLinkedList()
    {
        this.representants = new ArrayList<Sommet>();
        this.representing = new HashMap<Sommet, LinkedList<Sommet>>();
    }


    /**
     * Crée un nouvel ensemble dont le seul membre (et donc le représentant) est x.
     * Comme les ensembles sont disjoints, il faut que x ne soit pas déjà membre d'un autre ensemble.
     * @param x - le membre à ajouter
     */
    public void makeSet(Sommet x) throws IllegalArgumentException
    {
        if(find(x) != null){
            throw new IllegalArgumentException("Le sommet numéro "+x.getNumero()+" est déjà membre d'un autre ensemble");
        }

        LinkedList<Sommet> newEnsemble = new LinkedList<>();
        newEnsemble.add(x);
        this.representants.add(x);
        this.representing.put(x, newEnsemble);
    }


    /**
     * Réunit les ensembles qui contiennent x et y , disons Sx et
     * Sy , dans un nouvel ensemble qui est l'union de ces deux ensembles.
     * Les deux ensembles sont supposés être disjoints avant l'opération.
     * Le représentant de l'ensemble résultant est un membre quelconque de Sx ∪ Sy .
     * @param x - le premier membre
     * @param y - le second membre
     */
    public void union(Sommet x,Sommet y) throws IllegalArgumentException
    {
        Sommet representantX = find(x);
        LinkedList<Sommet> ensembleX = representing.get(representantX);

        Sommet representantY = find(y);
        LinkedList<Sommet> ensembleY = representing.get(representantY);

        if(!areDisjoints(ensembleX, ensembleY)){
            throw new IllegalArgumentException("Les ensembles contenant x et y ne sont pas disjoints");
        }

        this.updateRepresentants(representantY, representantX);
    }

    
    private void updateRepresentants(Sommet oldR,  Sommet newR){
        LinkedList<Sommet> representedByOld = this.representing.get(oldR);
        LinkedList<Sommet> representedByNew = this.representing.get(newR);

        representedByNew.addAll(representedByOld);

        this.representing.put(newR, representedByNew);
        this.representing.remove(oldR);

        int idOldR = oldR.getNumero()-1;
        this.representants.set(idOldR, newR);
        for(Sommet s : representedByOld){
            int idS = s.getNumero()-1;
            this.representants.set(idS, newR);
        }
    }


    /**
     * Retourne un pointeur vers le représentant de l'ensemble (unique) contenant x .
     * @param x - le membre dont on veut le représentant
     * @return le représentant de l'ensemble contenant x
     */
    public Sommet find(Sommet x)
    {
        if(x.getNumero() > this.representants.size()){
            return null;
        }
        int idX = x.getNumero()-1;
        return this.representants.get(idX);
    }


    /**
     * Vérifie si deux ensembles sont disjoints
     * @param ensembleX un ensemble
     * @param ensembleY un autre ensemble
     * @return true si les deux ensembles sont disjoints, false sinon
     */
    private boolean areDisjoints(LinkedList<Sommet> ensembleX, LinkedList<Sommet> ensembleY)
    {
        for(Sommet s : ensembleX){
            if(ensembleY.contains(s)){
                return false;
            }
        }
        return true;
    }
}
