package kruskal.unionfind;

import kruskal.Sommet;

public interface UnionFind
{

    /**
     * Crée un nouvel ensemble dont le seul membre (et donc le représentant) est x.
     * Comme les ensembles sont disjoints, il faut que x ne soit pas déjà membre d'un autre ensemble.
     * @param x - le membre à ajouter
     */
    public void makeSet(Sommet x) throws IllegalArgumentException;

    /**
     * Réunit les ensembles qui contiennent x et y , disons Sx et
     * Sy , dans un nouvel ensemble qui est l'union de ces deux ensembles.
     * Les deux ensembles sont supposés être disjoints avant l'opération.
     * Le représentant de l'ensemble résultant est un membre quelconque de Sx ∪ Sy .
     * @param x - le premier membre
     * @param y - le second membre
     */
    public void union(Sommet x,Sommet y) throws IllegalArgumentException;

    /**
     * Retourne un pointeur vers le représentant de l'ensemble (unique) contenant x .
     * @param x - le membre dont on veut le représentant
     * @return le représentant de l'ensemble contenant x
     */
    public Sommet find(Sommet x);
    
}
