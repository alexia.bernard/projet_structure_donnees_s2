package kruskal;

import java.util.Iterator;

/**
 * Une liste chaînée de noeuds.
 * La liste connait sa tête et sa queue.
 * Chaque noeud de la liste contient :
 *  une valeur
 *  un pointeur vers l'ensemble qui contient la valeur
 *  un pointeur vers le noeud suivant dans la liste.
 * Chaque objet associé à un ensemble a les pointeurs tête et queue pointant respectivement vers le premier et le dernier noeud.
 */
public class BetterLinkedList<E> implements Iterable<E>{

    /**
     * La tête de la liste
     */
    private BetterNode<E> head;

    /**
     * La queue de la liste
     */
    private BetterNode<E> queue;

    /**
     * La taille de la liste
     */
    private int size;

    /**
     * Constructeur
     */
    public BetterLinkedList(){
        this.head = null;
        this.queue = null;
        this.size = 0;
    }

    /**
     * Ajoute un élément à la fin de la liste
     * @param element - l'élément à ajouter
     */
    public void add(E element){
        BetterNode<E> node = new BetterNode<E>(element, this);
        if(this.head == null){
            this.head = node;
            this.queue = node;
        } else {
            this.queue.setNext(node);
            this.queue = node;
        }
        this.size++;
    }

    public boolean contains(E element){
        for(E currentElement : this){
            if(currentElement.equals(element)){
                return true;
            }
        }
        return false;
    }

    public void addAll(BetterLinkedList<E> other){
        // Link the list to the other
        if(this.queue != null && other.getHead() != null){
            this.queue.setNext(other.getHead());
            this.queue = other.getQueue();
        }
        // Foreach node in other, set the list to this
        BetterNode<E> current = other.getHead();
        while(current != null){
            current.setList(this);
            current = current.getNext();
        }
        this.size += other.size();
    }

    public int size(){
        return this.size;
    }

    public BetterNode<E> getHead(){
        return this.head;
    }

    public BetterNode<E> getQueue(){
        return this.queue;
    }

    public void setQueue(BetterNode<E> queue){
        this.queue = queue;
    }

    // ---------------------------- Implements Iterable<E> interface ---------------------------- //

    public Iterator<E> iterator() {
        return new BetterLinkedListIterator<E>(this.head);
    }

    private class BetterLinkedListIterator<F> implements Iterator<F> {

        private BetterNode<F> current;

        public BetterLinkedListIterator(BetterNode<F> head) {
            this.current = head;
        }

        public boolean hasNext() {
            return this.current != null;
        }

        public F next() {
            F element = this.current.getValue();
            this.current = this.current.getNext();
            return element;
        }
    }
}
