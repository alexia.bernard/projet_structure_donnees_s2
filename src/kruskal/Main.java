package kruskal;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import kruskal.unionfind.UF_BetterLinkedList;
import kruskal.unionfind.UF_Foret;
import kruskal.unionfind.UF_SimpleLinkedList;

public class Main {
    // Constante pour le nombre de sommets d'un petit graphe
    private static final int SMALL = 10;

    // Constante pour le nombre de sommets d'un graphe moyen
    private static final int MEDIUM = 1000;

    /* Constante pour le nombre de sommets d'un graphe grand */
    private static final int LARGE = 10000;

    /*
     * Fonction pour générer un graphe à partir dun fichier
     */
    private static Graphe generateGrapheFromFile(String filename)
    {
        System.out.println("Génération du graphe à partir du fichier...");
        try{
            List<int[]> graphValues = getGraphValuesFromFile(filename);
            int nbSommets = graphValues.size();

            List<Sommet> sommets = new ArrayList<Sommet>(nbSommets);
            List<Arete> aretes = new ArrayList<Arete>();

            // Construction liste sommets
            for (int i = 1; i <= nbSommets; i++) {
                sommets.add(new Sommet(i));
            }

            // Construction liste aretes
            for (int i = 0; i < nbSommets; i++) {
                for (int j = 0; j < nbSommets; j++) {
                    if (graphValues.get(i)[j] != 0) {
                        int poids = graphValues.get(i)[j];
                        aretes.add(new Arete(poids, sommets.get(i), sommets.get(j)));
                    }
                }
            }

            System.out.println("Graphe généré à partir du fichier !");

            return new Graphe(sommets, aretes);

        } catch(IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }

    private static List<int[]> getGraphValuesFromFile(String filename) throws IOException
    {
        BufferedReader lecteurAvecBuffer=null;
		String ligne;
        List<int[]> grapheValues = new LinkedList<int[]>();
		// String exempleCheminWindows = "C:\\Users\\Eric Soutil\\Desktop\\essai.csv";
		// String exempleCheminLinux = "/home/Maxime/Code/SDD/projet_structure_donnees_s2/Docs/graphe.txt";
		try {
			lecteurAvecBuffer = new BufferedReader(new FileReader(filename));
            
			while((ligne = lecteurAvecBuffer.readLine())!=null) {
				// ou traitement de la ligne : Exemple de split
				String[] tab = ligne.split(" ");
				int[] valeurLigne = new int[tab.length];
				for (int i = 0; i < valeurLigne.length; i++) {
					valeurLigne[i] = Integer.parseInt(tab[i]);
				}
                grapheValues.add(valeurLigne);
			}
			lecteurAvecBuffer.close();
		}catch(FileNotFoundException exc) {
			System.out.println("Erreur lors de l'ouverture du fichier");
		}

        return grapheValues;
    }

    /**
     *  Génère les graphes de test avec les tailles suivantes :
     *  <ul>
     *      <li> SMALL peu dense: 10 sommets, 30 arêtes </li>
     *      <li> SMALL dense: 10 sommets, 33 arêtes </li>
     *      <li> MEDIUM peu dense: 1000 sommets, 3 000 arêtes </li>
     *      <li> MEDIUM dense: 1000 sommets, 3 333 arêtes </li>
     *      <li> LARGE : 10000 sommets, 30 000 arêtes </li>
     *      <li> LARGE dense: 10000 sommets, 33 333 333 arêtes </li>
     * </ul>
     * @return la liste des graphes générés
     */
    private static List<Graphe> generateGraphs(){
        System.out.println("Création des graphes...");
        List<Graphe> graphs = List.of(
            new Graphe(SMALL, 3*SMALL),
            new Graphe(SMALL, (SMALL*SMALL)/3),
            new Graphe(MEDIUM, 3*MEDIUM),
            new Graphe(MEDIUM, (MEDIUM*MEDIUM)/3),
            new Graphe(LARGE, 3*LARGE),
            new Graphe(LARGE, (LARGE*LARGE)/3)
        );
        System.out.println("Graphes créés !");
        return graphs;
    }
    
    /**
     * Méthode principale du programme
     * @param args les arguments passés au programme : le nom du fichier modélisant un graphe (optionnel)
     */
    public static void main(String[] args) {
        // Vérification des arguments
        if(args.length > 1){
            System.out.println("Usage: java Main [filename]");
            System.exit(1);
        }

        // Génération des graphes
        List<Graphe> graphs = null;
        if(args.length == 1){
            graphs = List.of(generateGrapheFromFile(args[0]));
        }else{
            graphs = generateGraphs();
        }

        System.out.println("Exécution des algorithmes de Kruskal...");
        // Exécution des algorithmes de Kruskal sur les graphes
        Scanner sc = new Scanner(System.in);
        for(Graphe g : graphs){
            Kruskal kruskal1 = new Kruskal(new UF_SimpleLinkedList());
            Kruskal kruskal2 = new Kruskal(new UF_BetterLinkedList());
            Kruskal kruskal3 = new Kruskal(new UF_Foret());
            System.out.println(
                "\n\n\u001B[31m------------------------------------------------------------ "
                + g.getNombreSommets()+" sommets, "+g.getNombresAretes()
                +" aretes ------------------------------------------------------------\u001B[0m"
            );
            Arbre a1 = kruskal1.run(g);
            Arbre a2 = kruskal2.run(g);
            Arbre a3 = kruskal3.run(g);

            // Affichage de l'arbre obtenu
            displayTree(sc, a1, a2, a3);
        }
        sc.close();
        System.out.println("\nFin du programme d_(￣◇￣)_b");
    }


    /**
     * Affiche les arbres obtenus par les algorithmes de Kruskal
     * @param a1 l'arbre obtenu par l'algorithme de Kruskal avec la structure UFSimpleLinkedList
     * @param a2 l'arbre obtenu par l'algorithme de Kruskal avec la structure UFLinkedListHeadQueue
     * @param a3 l'arbre obtenu par l'algorithme de Kruskal avec la structure UFForet
     */
    private static void displayTree(Scanner sc, Arbre a1, Arbre a2, Arbre a3)
    {
        System.out.println("\n\t  Voulez-vous afficher un des trois arbres obtenus ? (y/n)");
        System.out.print("\t  > ");
        String answer = sc.nextLine();
        if(answer.equals("y")){
            System.out.println("\t  Quel arbre voulez-vous afficher ? (1/2/3)");
            System.out.print("\t  > ");
            answer = sc.nextLine();
            eraseLine(5);
            if(answer.equals("1")){
                System.out.println("\n\t  "+a1.toString().replace("\n","\n\t  "));
            }else if(answer.equals("2")){
                System.out.println("\n\t  "+a2.toString().replace("\n","\n\t  "));
            }else if(answer.equals("3")){
                System.out.println("\n\t  "+a3.toString().replace("\n","\n\t  "));
            }
            // Pause program 1s
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
            }
        }else{
            eraseLine(3);
        }
    }

    /**
     * Efface nb lignes dans la console
     * @param nb le nombre de lignes à effacer
     */
    private static void eraseLine(int nb){
        for(int i = 0; i < nb; i++){
            System.out.print("\033[1A");
            System.out.print("\033[2K");
        }
    }
}
