package kruskal;
public class BetterNode<E>
{
    /**
     * La liste à laquelle appartient le noeud
     */
    private BetterLinkedList<E> list;
    private E value;
    private BetterNode<E> next;

    public BetterNode(E value, BetterLinkedList<E> list){
        this.value = value;
        this.next = null;
        this.list = list;
    }

    public E getValue() {
        return this.value;
    }

    public BetterNode<E> getNext() {
        return this.next;
    }

    public void setNext(BetterNode<E> next) {
        this.next = next;
    }

    public BetterLinkedList<E> getList(){
        return this.list;
    }

    public void setList(BetterLinkedList<E> list){
        this.list = list;
    }

    @Override
    public String toString(){
        return this.getValue().toString();
    }
}
