package kruskal;
import java.util.LinkedList;
import java.util.List;

public class NoeudArbre {

    /**
     * La liste des successeurs
     */
    private List<NoeudArbre> adjacents;

    /**
     * La valeur du noeud
     */
    private int value;

    /**
     * 
     * @param previous - le précédent
     * @param listeNext - les successeurs
     * @param value - la valeur
     */
    public NoeudArbre(List<NoeudArbre> listeNext, int value){
        this.adjacents = listeNext;
        this.value = value;
    }

    /**
     * 
     * @param previous - le précédent
     * @param adjacents - les successeurs
     * @param value - la valeur
     */
    public NoeudArbre(int value){
        this.value = value;
        this.adjacents = new LinkedList<NoeudArbre>();
    }

    /**
     * La liste des successeurs
     * @return la liste des successeurs
     */
    public List<NoeudArbre> getAdjacents(){
        return this.adjacents;
    }

    /**
     * Renvoie a valeur
     * @return la valeur
     */
    public int getValue(){
        return this.value;
    }

    /**
     * Change la valeur du noeud
     * @param value
     */
    public void setValue(int value){
        this.value = value;
    }

    /**
     * Ajoute le noeud n à la liste des successeurs
     * @param n - le nouveau successeur
     */
    public void addAdjacent(NoeudArbre n){
        this.adjacents.add(n);
    }
}
