package kruskal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import kruskal.unionfind.UnionFind;

public class Kruskal{
    /**
     * Une structure de données UnionFind
     */
    private UnionFind unionFind;

    /**
     * Constructeur initialisant la structure de données UnionFind
     * @param unionFind
     */
    public Kruskal(UnionFind unionFind)
    {
        this.unionFind = unionFind;
    }

    /**
     * Exécute l'algorithme de Kruskal sur le graphe g
     * @param g - le graphe
     */
    public Arbre run(Graphe g)
    {
        System.out.println("\nAlgorithme de Kruskal avecl l'implémentation UnionFind : \u001B[1m"+unionFind.getClass().getName()+"\u001B[0m");
        // init time to measure execution time in microseconds
        long startTime = System.nanoTime();
        double durationTri = 0;

        // Start Kruskal algorithm
        int k = 0;
        List<Arete> ensembleAretes = g.getAretes();
        List<Arete> aretesArbre = new LinkedList<Arete>();
        // Tri de la liste d'arêtes par ordre croissant de poids
        long startTimeTri = System.nanoTime();
        sortAreteList(ensembleAretes);
        durationTri = ((double)(System.nanoTime() - startTimeTri)) /1000.0;
        displayDuration(durationTri);

        // init unionFind
        long startTimeInitUF = System.nanoTime();
        System.out.println("\tInit UnionFind ...");
        for(Sommet s : g.getSommets()){
            unionFind.makeSet(s);
        }
        durationTri = ((double)(System.nanoTime() - startTimeInitUF)) /1000.0;
        displayDuration(durationTri);

        // Start the core of Kruskal algorithm
        long startTimeCore = System.nanoTime();
        System.out.println("\tStart Kruskal algorithm (mainloop) ...");
        while(k < (g.getNombresAretes())){
            Arete a = ensembleAretes.get(k);
            Sommet s1 = a.getSommet1();
            Sommet s2 = a.getSommet2();
            if(unionFind.find(s1) != unionFind.find(s2)){
                unionFind.union(s1, s2);
                aretesArbre.add(a);
            }
            k++;
        }
        durationTri = ((double)(System.nanoTime() - startTimeCore)) /1000.0;
        displayDuration(durationTri);

        // Display runtime
        long endTime = System.nanoTime();
        double duration = ((double)(endTime - startTime)) /1000.0;
        Arbre returnArbre = new Arbre(aretesArbre,g.getSommets());
        displayDurationTime(g.getNombreSommets(), g.getNombresAretes(), duration);
        displayResult(returnArbre);

        return returnArbre;
    }


    /**
     * Tri par dénombrement de la liste pour ranger les arêtes par ordre croissant de poids
     * @param liste - la liste à trier
     * @return la liste triée par ordre croissant de poids
     */
    private void sortAreteList(List<Arete> liste)
    {
        System.out.println("\tTri par dénombrement de la liste d'arêtes ...");
        int maxVal = Arete.getMax();
        ArrayList<LinkedList<Arete>> tab = new ArrayList<LinkedList<Arete>>();
        // Initialisation de la liste triée avec des LinkedList
        for(int i = 0; i < maxVal+1; i++){
            tab.add(new LinkedList<>());
        }
        // Remplissage de la liste tab avec les arêtes, arete de poids i va dans la liste chainée à la position i de tab
        for(Arete a : liste){
            tab.get(a.getPoids()).add(a);
        }

        // Remplissage de la liste triée
        int i = 0;
        // Pour chaque liste dans tab
        for(int j = 0; j < maxVal+1; j++){
            // Tant que la liste n'est pas vide ni null
            while(tab.get(j) != null && !tab.get(j).isEmpty()){
                // On ajoute le premier élément de la liste tab[j] à la liste triée
                liste.set(i, tab.get(j).removeFirst());
                i++;
            }
        }
    }

    /**
     * Affiche le temps d'exécution de l'algorithme de Kruskal sur le graphe g de nbSommets sommets et nbAretes arêtes
     * Affiche le temps en microsecondes, millisecondes ou secondes selon la durée d'exécution
     * @param nbSommets - le nombre de sommets du graphe
     * @param nbAretes - le nombre d'arêtes du graphe
     * @param duration - le temps d'exécution de l'algorithme en microsecondes
     */
    private void displayDurationTime(int nbSommets, int nbAretes, double duration){
        String output0 = String.format(
            "\n\t%sRésultats de l'lgorithme de Kruskal [%d sommets; %d aretes]%s",
            "\u001B[1m",nbSommets, nbAretes, "\u001B[0m"
            );
        String totalDuration = buildTimeString(duration);
        String output1 = String.format(
            "\t  *  Temps de calcul total --->  %s %s %s",
            "\u001B[32m", totalDuration,"\u001B[0m"
        );

        System.out.println(output0);
        System.out.println(output1);
    }

    private void displayDuration(double duration){
        String durationString = buildTimeString(duration);
        String output = String.format(
            "\t  *  --->  %s %s %s",
            "\u001B[31m", durationString,"\u001B[0m"
        );
        System.out.println(output);
    }

    private void displayResult(Arbre arbre){
        String output = String.format(
            "\t  *  Arbre de poids total :  --->  %s %d %s",
            "\u001B[32m", arbre.getPoids(), "\u001B[0m"
        );

        System.out.println(output);
    }

    private String buildTimeString(double duration){
        String timeUnit = "µs";

        if(duration > 1000000.0){
            duration /= 1000000.0; // microseconds to seconds
            timeUnit = "s";
        }else if(duration > 1000.0){
            duration /= 1000.0; // microsecords to milliseconds
            timeUnit = "ms";
        }

        return String.format("%,.3f %s", duration, timeUnit);
    }
    
}
