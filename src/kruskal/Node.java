package kruskal;
public class Node<E> {

    private E value;
    private Node<E> next;

    public Node(E v) {
        this.value = v;
        this.next = null;
    }

    public E getValue() {
        return this.value;
    }

    public Node<E> getNext() {
        return this.next;
    }

    public void setNext(Node<E> next) {
        this.next = next;
    }
}
    

