package kruskal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Arbre extends Graphe{
    /**
     * La tête de l'arbre
     */
    private NoeudArbre head;

    /**
     * La liste des noeuds de l'arbre
     */
    List<NoeudArbre> noeuds;

    /**
     * La taille de l'arbre (hauteur)
     */
    private int size;

    /**
     * Le poids de l'arbre
     */
    private int poids;

    /**
     * 
     * @param head - la tête de l'arbre
     * @param size - la taille de l'arbre
     * @param listeAretes - la liste des arêtes
     * @param listeSommets - la liste des sommets
     */
    public Arbre(List<Arete> listeAretes, List<Sommet> listeSommets){
        super(listeSommets, listeAretes);

        this.noeuds = defineNoeuds(listeAretes, listeSommets);
        this.head = defineHead(this.noeuds);
        this.poids = 0;
        for(Arete a : listeAretes){
            this.poids += a.getPoids();
        }
    }


    /**
     * Création des noeuds de l'arbre à partir des sommets et des arêtes
     * 
     * @param listeAretes - la liste des arêtes
     * @param listeSommets - la liste des sommets
     * @return la liste des noeuds
     */
    private List<NoeudArbre> defineNoeuds(List<Arete> listeAretes, List<Sommet> listeSommets){
        List<NoeudArbre> noeuds = new ArrayList<NoeudArbre>(listeSommets.size());
        for(Sommet s : listeSommets){
            noeuds.add(new NoeudArbre(s.getNumero()));
        }
        for(Arete a : listeAretes){
            int num1 = a.getSommet1().getNumero();
            int num2 = a.getSommet2().getNumero();
            noeuds.get(num1-1).addAdjacent(noeuds.get(num2-1));
            noeuds.get(num2-1).addAdjacent(noeuds.get(num1-1));
        }
        return noeuds;
    }


    /**
     * Création de la tête de l'arbre
     * @param noeuds
     * @return la tête de l'arbre
     * @throws RuntimeException - si la tête n'est pas trouvée
     */
    private NoeudArbre defineHead(List<NoeudArbre> noeuds){
        for(NoeudArbre n : noeuds){
            if( n.getAdjacents().size() == 1){
                return n;
            }
        }
        throw new RuntimeException("No head found");
    }

    /**
     * Renvoie la tête de l'arbre
     * @return la tête de l'arbre
     */
    public NoeudArbre getHead(){ 
        return this.head;
    }

    /**
     * Renvoie la hauteur de l'arbre
     * @return la hauteur de l'arbre
     */
    public int getSize(){
        return this.size;
    }

    /**
     * Renvoie le poids de l'arbre
     * @return le poids de l'arbre
     */
    public int getPoids(){
        return this.poids;
    }

    /**
     * Change la hauteur de l'arbre
     * @param size - la nouvelle hauteur de l'arbre
     */
    public void setSize(int size){
        this.size = size;
    }

    @Override
    public String toString(){
        if (this.head == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Poids : "+this.poids+"\n\n");
        buildTreeStringDFS(this.head, sb, 0, new HashSet<NoeudArbre>());
        sb.append("\nPoids : "+this.poids+"\n");
        return sb.toString();
    }
    
    private void buildTreeStringDFS(NoeudArbre node, StringBuilder sb, int depth, Set<NoeudArbre> visited) {
        if (node == null) {
            return;
        }

        for (int i = 0; i < depth-1; i++) {
            sb.append("|  ");
        }
        if(depth > 0){
            sb.append("↳  "+node.getValue());
        } else {
            sb.append(node.getValue());
        }
        sb.append("\n");
        visited.add(node);

        for (NoeudArbre child : node.getAdjacents()) {
            if (visited.contains(child)) {
                continue;
            }
            buildTreeStringDFS(child, sb, depth + 1, visited);
        }
    }

}
