package kruskal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe représentant un graphe
 * 
 * @author Aléxia Bernard & Maxime Cardinale
 */
public class Graphe {

    /**
     * Le nombre de sommet du graphe
     */
    protected int nbSommets;

    /**
     * Le nombre d'arêtes du graphe
     */
    protected int nbAretes; // nb aretes

    /**
     * La liste de tous les sommets du graphe
     */
    protected List<Sommet> sommets;

    /**
     * La liste de toutes les aretes du graphe
     */
    protected List<Arete> aretes;

    /**
     * Constructeur créant un graphe aléatoire de nbSommets sommets et nbAretes arêtes
     * @param nbSommets - le nombre de sommet du graphe
     * @param nbAretes - le nombre d'arêtes du graphe
     */
    public Graphe(int nbSommets, int nbAretes){
        System.out.println("Création d'un graphe de " + nbSommets + " sommets et de " + nbAretes + " arêtes.");
        this.nbSommets = nbSommets;
        this.nbAretes = nbAretes;
        this.initSommets();
        this.initAretes();
    }

    /**
     * Constructeur créant un graphe à partir d'une liste de sommets et d'une liste d'arêtes
     * @param sommets - la liste des sommets
     * @param aretes - la liste des aretes
     */
    public Graphe(List<Sommet> sommets, List<Arete> aretes){
        this.sommets = sommets;
        this.aretes = aretes;
        this.nbSommets = sommets.size();
        this.nbAretes = aretes.size();
    }

    /**
     * Renvoie le nombre de sommet du graphe
     *
     * @return le nombre de sommet
     */
    public int getNombreSommets(){ 
        return this.nbSommets; 
    }

    /**
     * Renvoie le nombre d'arêtes du graphe
     *
     * @return le nombre d'arêtes
     */
    public int getNombresAretes(){ 
        return this.nbAretes; 
    }

    /**
     * Renvoie la liste de tous les sommets du graphes
     * 
     * @return la liste des sommets
     */
    public List<Sommet> getSommets(){ 
        return this.sommets; 
    }

    /**
     * Renvoie la liste de toutes les aretes du graphes
     * 
     * @return la liste des aretes
     */
    public List<Arete> getAretes(){ 
        return this.aretes; 
    }

    /**
     * Renvoie l'arête numéro i
     * @param i - le numéro de l'arête à récupérer
     * @return l'arête numéro i
     */
    public Arete getArete(int i){
        return this.aretes.get(i);
    }

    /**
     * Initialise les sommets du graphe
     */
    private void initSommets(){
        this.sommets = new ArrayList<Sommet>(this.nbSommets);
        for(int i=0; i<this.nbSommets; i++)
            this.sommets.add(new Sommet(i+1));
    }

     /**
     * Initialise les aretes du graphe
     */
    private void initAretes(){        
        // Création des aretes de base
        this.aretes = new ArrayList<Arete>(this.nbAretes);
        for(int i = 0; i < this.nbSommets-1; i++)
            this.aretes.add(new Arete(this.sommets.get(i), this.sommets.get(i+1)));

        // Création de toutes les aretes possibles saufs celle de base
        List<Arete> all = new ArrayList<Arete>();
        for (int i = 0; i < nbSommets - 1; i++) {
            for (int j = i + 1; j < nbSommets; j++) {
                if(i!=j && j!=i+1){
                    all.add(new Arete(this.sommets.get(i), this.sommets.get(j)));
                }
            }
        }

        // On mélange la liste des aretes
        Collections.shuffle(aretes);
        // On prend les sommets nécéssaires de la list all mélangée
        int nbActuel = this.aretes.size();
        for(int i = 0; i < this.nbAretes-nbActuel; i++){
            this.aretes.add(all.get(i));
        }
    }

    /**
     * Ajoute une arête entre deux sommets 
     *
     * @param d - le sommet qui est en début de l'arête
     * @param f - le sommet qui est en fin d'arête
     */
    public void addArrete(Sommet d, Sommet f){
        this.aretes.add(new Arete(d, f));
    }

    /**
     * Ajoute l'arête mise en argument
     * @param a - l'arête à ajouter
     */
    public void addArete(Arete a){
        this.aretes.add(a);
    }
}
