package kruskal;
/**
 * Classe Arete représentant une arête du graphe
 * 
 * @author Aléxia Bernard & Maxime Cardinale
 */
public class Arete {
    
    /**
     * Le poids de l'arête
     */
    private int poids;

    /**
     * Une des extrémités de l'arête
     */
    private Sommet sommet1;

    /**
     * L'autre extrémité de l'arête
     */
    private Sommet sommet2;

    /**
     * Constante minimale pour la génération aléatoire du poids
     */
    private final static int MIN = 1;

    /**
     * Constante maximale pour la génération aléatoire du poids
     */
    private final static int MAX = 1000;


    /**
     * Constructeur de l'arête
     * @param sommet1 - une des extrémités de l'arête
     * @param sommet2 - l'autre extrémité de l'arête
     */
    public Arete(Sommet sommet1, Sommet sommet2){
        this.poids = MIN + (int)(Math.random()*((MAX-MIN)+1));
        this.sommet1 = sommet1;
        this.sommet2 = sommet2;
    }

    /**
     * Constructeur de l'arête
     * @param poids - le poids de l'arête
     * @param sommet1 - une des extrémités de l'arête
     * @param sommet2 - l'autre extrémité de l'arête
     */
    public Arete(int poids, Sommet sommet1, Sommet sommet2){
        this.poids = poids;
        this.sommet1 = sommet1;
        this.sommet2 = sommet2;
    }

    /**
     * Renvoie le poids de l'arête
     * @return le poids de l'arête
     */
    public int getPoids(){
        return this.poids;
    }

    /**
     * Renvoie une des extrémités de l'arête
     * @return une des extrémités de l'arête
     */
    public Sommet getSommet1(){
        return this.sommet1;
    }

    /**
     * Renvoie l'autre extrémité de l'arête
     * @return l'autre extrémité de l'arête
     */
    public Sommet getSommet2(){
        return this.sommet2;
    }

    static int getMax(){
        return MAX;
    }

    @Override
    public String toString(){
        return "(" + this.sommet1.getNumero() + " <--> " + this.sommet2.getNumero() + ")";
    }
}
